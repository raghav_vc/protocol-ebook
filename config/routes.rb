Rails.application.routes.draw do
  root 'home#landing_page'

  get '/books/:id' => 'books#show'
  get '/search' => 'books#search'
  get '/category/:id' => 'category#show'
  get '/buy/:id' => 'buy#submit'
  get '/read/:id' => 'cart#read'
  get '/signup/:id' => 'signup#sign_in'
  get '/login:/id' => 'login#log'
  get 'books/:id/buy' => 'buy#submit'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
