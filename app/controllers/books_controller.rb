class BooksController < ApplicationController
  def show
    @book = Book.find(params[:id])
  end

  def search
    @books = Book.where("name LIKE ? OR description  LIKE ?", "%#{params[:query].to_s}%", "%#{params[:query].to_s}%")
  end
end
