class Book < ApplicationRecord

  belongs_to :category
  has_many :author_book_relations
	has_many :authors, through: :author_book_relations
  has_many :orders

end
