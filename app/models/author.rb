class Author < ApplicationRecord
  has_many :author_book_relations
	has_many :books, through: :author_book_relations

end
