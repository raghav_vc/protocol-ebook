class CreateAuthorBookRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :author_book_relations do |t|
      t.references :book, index: true, foreign_key: true
      t.references :author, index: true, foreign_key: true
      t.timestamps  null: false
    end
  end
end
