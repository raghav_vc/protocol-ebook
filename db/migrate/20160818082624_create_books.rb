class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
        t.references       :category, index: true, foreign_key: true
    	t.string           :name, null: false
        t.string           :image_src
    	t.string           :description
    	t.integer          :edition
    	t.string           :language
    	t.integer          :size
    	t.boolean          :available
        t.string           :url
    	t.string           :published_on
    	t.integer          :price, null: false
    	t.integer          :discount
    	t.integer          :pages, null: false
    	t.integer          :sold
    	t.boolean          :allow_ratings, null: false, default: true
    	t.boolean          :status

      t.timestamps null: false
    end
  end
end
