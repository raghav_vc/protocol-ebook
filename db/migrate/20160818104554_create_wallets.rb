class CreateWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :wallets do |t|
    	t.references	:user, index: true, foreign_key: true
      t.string  :card_name
      t.string  :card_type
      t.string  :card_num
      t.string  :card_cvv
    	t.string :billing_address
    	t.float  :balance
      t.timestamps  null: false
    end
  end
end
