class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
    	t.string           :first_name, null: false
    	t.string           :last_name
    	t.string           :email
    	t.string           :password
    	t.integer          :phone, limit: 10

      t.timestamps  null: false
    end
  end
end
