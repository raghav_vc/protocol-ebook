class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :user, index: true, foreign_key: true
      t.references :book, index: true, foreign_key: true

      t.float    :price 
      t.string   :payment_status
      t.date     :paid_on

      t.timestamps  null: false
    end
  end
end
