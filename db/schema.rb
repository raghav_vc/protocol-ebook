# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160819064355) do

  create_table "author_book_relations", force: :cascade do |t|
    t.integer  "book_id"
    t.integer  "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_author_book_relations_on_author_id"
    t.index ["book_id"], name: "index_author_book_relations_on_book_id"
  end

  create_table "authors", force: :cascade do |t|
    t.string   "first_name", null: false
    t.string   "last_name"
    t.string   "awards"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "books", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "name",                         null: false
    t.string   "image_src"
    t.string   "description"
    t.integer  "edition"
    t.string   "language"
    t.integer  "size"
    t.boolean  "available"
    t.string   "url"
    t.string   "published_on"
    t.integer  "price",                        null: false
    t.integer  "discount"
    t.integer  "pages",                        null: false
    t.integer  "sold"
    t.boolean  "allow_ratings", default: true, null: false
    t.boolean  "status"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["category_id"], name: "index_books_on_category_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "book_id"
    t.float    "price"
    t.string   "payment_status"
    t.date     "paid_on"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["book_id"], name: "index_orders_on_book_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "first_name",            null: false
    t.string   "last_name"
    t.string   "email"
    t.string   "password"
    t.integer  "phone",      limit: 10
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "wallets", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "card_name"
    t.string   "card_type"
    t.string   "card_num"
    t.string   "card_cvv"
    t.string   "billing_address"
    t.float    "balance"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["user_id"], name: "index_wallets_on_user_id"
  end

end
