Category.create([
  { name: "Academic"},
  { name: "Fiction"},
  { name: "Entrance Exam"},
  { name: "Children"},
  { name: "Comic"},
  { name: "Biography"},
  {name: "Aerospace"},
  {name: "Computer Science and Engineering"},
  {name: "Mechanical Engineering"}
])



Book.create([
  {url: "assets/books/clrs.pdf", image_src: "assets/images/books/Clrs3.jpeg", name: "Introduction To Algorithms", description: "Cool book on algorithms", edition: 3, language: "English", size: 1479, available: true, published_on: "31 Jul 2009", price: 750, discount: 30, pages: 1312, category_id: Category.first.id, sold: 21030, allow_ratings: true, status: true},
  {url: "assets/books/ruby.pdf", image_src: "assets/images/books/51L-xznU7EL._AC_UL320_SR248,320_.jpg", name: "Ruby Programming : The Well-Grounded Rubyist", description: "A step by step approach to learn ruby", edition: 2, language: "English", size: 5393, available: true, published_on: "22 May 2008", price: 250, discount: 5, pages: 560, category_id: Category.first.id, sold: 5002, allow_ratings: true, status: true},
  {url: "assets/books/rails.pdf", image_src: "assets/images/books/rails.jpg", name: "Ruby On Rails Tutorial", description: "Awesome way to learn rails", edition: 3, language: "English", size: 9416, available: true, published_on: "12 Aug 2014", price: 355, discount: 15, pages: 614, category_id: Category.first.id, sold: 9825, allow_ratings: true, status: true},


  {url: "assets/books/meluha.pdf", image_src: "assets/images/books/51m0wvVXZ1L._SX319_BO1,204,203,200_.jpg"  ,name: "The Immortals of Meluha", description: "Great way to learn abot Indian mythology", edition: 1, language: "English", size: 2473, available: true, published_on: "06 Feb 2010", price: 140, discount: 20, pages: 390, category_id: Category.second.id, sold: 10008, allow_ratings: true, status: true},
  {url: "assets/books/secret.pdf", image_src: "assets/images/books/The_Secret_of_the_Nagas.jpg", name: "The Secret of The Nagas", description: "Shiva is the coolest guy ever", edition: 1, language: "English", size: 2477, available: true, published_on: "21 Jul 2011", price: 210, discount: 10, pages: 512, category_id: Category.second.id, sold: 23480, allow_ratings: true, status: true},
  {url: "assets/books/oath.pdf",image_src: "assets/images/books/The_Oath_of_the_Vayuputras.jpg", name: "The Oath of Vayuputhras", description: "Simple, yet amazing", edition: 1, language: "English", size: 3133, available: true, published_on: "12 Aug 2015", price: 310, discount: 10, pages: 672, category_id: Category.second.id, sold: 9723, allow_ratings: true, status: true},


  {url: "assets/books/gre.pdf", image_src: "assets/images/books/51PoPf5ndsL._SX258_BO1,204,203,200_.jpg", name: "Official GRE Verbal Reasoning Practice Questions with Practice for the Analytical Writing Measure ", description: "Best book for GRE", edition: 1, language: "English", size: 2257, available: true, published_on: "12 May 2010", price: 150, discount: 10, pages: 256, category_id: Category.third.id, sold: 7323, allow_ratings: true, status: true},
  {url: "assets/books/gate.pdf", image_src: "assets/images/books/51eiylIQF0L._SX380_BO1,204,203,200_.jpg", name: "GATE Guide Mechanical Engg. 2016", description: "Clear the GATE exam right away", edition: 1, language: "English", size: 4106, available: true, published_on: "01 Jan 2016", price: 270, discount: 5, pages: 712, category_id: Category.third.id, sold: 3223, allow_ratings: true, status: true},
  {url: "assets/books/maths.pdf", image_src: "assets/images/books/maths.jpeg", name: "Course in Mathematics 2D geometry", description: "Maths beoming easy", edition: 1, language: "English", size: 6991, available: true, published_on: "23 Oct 2015", price: 215, discount: 10, pages: 562, category_id: Category.third.id, sold: 1232, allow_ratings: true, status: true},

  {url: "assets/books/rusk.pdf", image_src: "assets/images/books/bina.jpeg", name: "A long walk for Bina", description: "Kids will love it", edition: 1, language: "English", size: 2482, available: true, published_on: "21 Nov 2011", price: 45, discount: 10, pages: 120, category_id: Category.fourth.id, sold: 6252, allow_ratings: true, status: true},
  {url: "assets/books/malgudi.pdf", image_src: "assets/images/books/Malgudi_Days.jpg", name: "Malgudi Days", description: "Good days", edition: 1, language: "English", size: 535, available: true, published_on: "05 Sep 2009", price: 100, discount: 10, pages: 365, category_id: Category.fourth.id, sold: 1231, allow_ratings: true, status: true},
  {url: "assets/books/grandma.pdf", image_src: "assets/images/books/5196QTnmADL._SX324_BO1,204,203,200_.jpg", name: "Grandma's Bag of Stories", description: "Stories that will make the children sleep", edition: 1, language: "English", size: 580, available: true, published_on: "11 Feb 2012", price: 35, discount: 10, pages: 192, category_id: Category.fourth.id, sold: 997, allow_ratings: true, status: true},


  {url: "assets/books/avengers.pdf", image_src: "assets/images/books/index.jpeg", name: "The Avengers - The S.H.I.E.L.D. Files", description: "Will the Avengers win", edition: 1, language: "English", size: 2482, available: true, published_on: "15 Jan 2007", price: 15, discount: 20, pages: 24, category_id: Category.fifth.id, sold: 560, allow_ratings: true, status: true},
  {url: "assets/books/tintin.pdf", image_src: "assets/images/books/tintin.jpeg", name: "The Adventures of Tintin : Cigars of the Pharaoh", description: "Best Tintin book", edition: 1, language: "English", size: 1876, available: true, published_on: "12 Jan 2016", price: 30, discount: 5, pages: 64, category_id: Category.fifth.id, sold: 4123, allow_ratings: true, status: true},
  {url: "assets/books/wimpy.pdf", image_src: "assets/images/books/51IU4xLoboL.jpg", name: "The Wimpy Kid Do-It-Yourself Book", description: "Step by step approach", edition: 1, language: "English", size: 3463, available: true, published_on: "27 May 2016", price: 40, discount: 0, pages: 100, category_id: Category.fifth.id, sold: 672, allow_ratings: true, status: true},


  {url: "assets/books/apj.pdf", image_src: "assets/images/books/Wings_of_Fire_by_A_P_J_Abdul_Kalam_Book_Cover.jpg", name: "Wings of Fire: An Autobiography", description: "Insight into the life of APJ", edition: 1, language: "English", size: 6074, available: true, published_on: "30 Jan 2002", price: 65, discount: 30, pages: 196, category_id: Category.find(id=6).id, sold: 12462, allow_ratings: true, status: true},
  {url: "assets/books/adolf.pdf", image_src: "assets/images/books/41tTZSUxoyL._SY344_BO1,204,203,200_.jpg", name: "Mein Kampf", description: "Gives a different perception on Hitler", edition: 1, language: "English", size: 1779, available: true, published_on: "22 Sep 1988", price: 60, discount: 50, pages: 264, category_id: Category.find(id=6).id, sold: 8724, allow_ratings: true, status: true},
  {url: "assets/books/anne.pdf", image_src: "assets/images/books/519HKX9M69L._SY344_BO1,204,203,200_.jpg", name: "The Diary Of A Young Girl", description: "War is hell", edition: 1, language: "English", size: 1197, available: true, published_on: "22 May 1996", price: 90, discount: 40, pages: 120, category_id: Category.find(id=6).id, sold: 7626, allow_ratings: true, status: true},

  {url: "", image_src: "assets/images/books/index.png", name: "Introduction To Algorithms - Part 2", description: "Advanced book on algorithms", edition: 1, language: "English", size: 4562, available: false, published_on: "01 Feb 2017", price: 750, discount: 0, pages: 1312, category_id: Category.first.id, sold: 0, allow_ratings: false, status: true},

  {url: "", image_src: "assets/images/books/index.png", name: "Advanced Ruby On Rails Applications", description: "Bring the RoR developer in you", edition: 1, language: "English", size: 2321, available: false, published_on: "12 Sep 2017", price: 225, discount: 0, pages: 812, category_id: Category.first.id, sold: 0, allow_ratings: false, status: true},

  {url: "assets/books/kane.pdf", image_src: "assets/images/books/kane.jpeg", name: "Kane and Abel", description: "Great story about 2 persons Kane and Abel", edition: 1, language: "English", size: 1839, available: true, published_on: "12 Jan 1979", price: 160, discount: 5, pages: 758, category_id: Category.second.id, sold: 2001, allow_ratings: true },

  {url: "assets/books/sachin.pdf", image_src: "assets/images/books/sachin.jpg", name: "Playing it my way", description: "Get to know about the life of Sachin Tendulkar", edition: 1, language: "English", size: 8046, available: true, published_on: "11 May 2015", price: 140, discount: 10, pages: 368, category_id: Category.find(id=6).id, sold: 6022, allow_ratings: true },

  {url: "assets/books/scion.pdf", image_src: "assets/images/books/scion.jpeg", name: "Scion of Ishvaku", description: "A different insight into Indian mythology", edition: 1, language: "English", size: 2091, available: true, published_on: "17 Jan 2016", price: 180, discount: 20, pages: 218, category_id: Category.second.id, sold: 812, allow_ratings: true },

  {url: "assets/Aero/Aircraft_structures.pdf", image_src: "assets/images/books/aircraft_structures.jpg", name: "Aircraft Structutes", description: "An interesting book on aircrafts", edition: 4, language: "English", size: 4840, available: true, published_on: "30 Jan 2002", price: 220, discount: 12, pages: 638, category_id: Category.find(id=7).id, sold: 812, allow_ratings: true },

  {url: "assets/Aero/Fluid_mech.pdf", image_src: "assets/images/books/fluid_mech.jpg", name: "Fluid mechanics", description: "A detailed explanation on fluid mechanics", edition: 7, language: "English", size: 28524, available: true, published_on: "30 Jan 2002", price: 180, discount: 11, pages: 862, category_id: Category.find(id=7).id, sold: 812, allow_ratings: true },

  {url: "assets/Aero/Fundamentals_of_aerodynamics.pdf", image_src: "assets/images/books/fundamentals_of_aero.jpeg", name: "Fundamentals of Aerodynamics", description: "A detailed explanation on fundamental of aerodynamics", edition: 2, language: "English", size: 25954, available: true, published_on: "30 Jan 2002", price: 280, discount: 16, pages: 772, category_id: Category.find(id=7).id, sold: 812, allow_ratings: true },

  {url: "assets/Aero/Gas_turbine_theory.pdf", image_src: "assets/images/books/gas.jpg", name: "Gas turbine theory", description: "The best book for gas turbine theory", edition: 4, language: "English", size: 25954, available: true, published_on: "30 Jan 2002", price: 264, discount: 13, pages: 442, category_id: Category.find(id=7).id, sold: 812, allow_ratings: true },

  {url: "assets/Aero/Introduction_to_flight.pdf", image_src: "assets/images/books/intro_flight.jpg", name: "Introduction to flight", description: "The best way to learn about flights", edition: 3, language: "English", size: 28595, available: true, published_on: "30 Jan 2002", price: 172, discount: 13, pages: 616, category_id: Category.find(id=7).id, sold: 812, allow_ratings: true },


  {url: "assets/CSE/coca.pdf", image_src: "assets/images/books/coca.jpeg", name: "Computer Organization and Architecture", description: "Gives the reader a detailed understanding on coca", edition: 8, language: "English", size: 3111, available: true, published_on: "30 Jan 2002", price: 122, discount: 11, pages: 774, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/Computer_graphics.pdf", image_src: "assets/images/books/cg.jpeg", name: "Computer graphics", description: "understanding of computer graphics made easy", edition: 2, language: "English", size: 21158, available: true, published_on: "30 Jan 2002", price: 322, discount: 18, pages: 652, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/Computer_Networks.pdf", image_src: "assets/images/books/tanen.jpg", name: "Computer Networks by Tanenbaum ", description: "understanding of networks made easy", edition: 5, language: "English", size: 8454, available: true, published_on: "30 Jan 2002", price: 222, discount: 8, pages: 933, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/Data_Communications_and_Networking_By_Behrouz_A_Forouzan.pdf", image_src: "assets/images/books/networking.jpeg", name: "Data Communications and Networking", description: "Indepth knowledge on data communication", edition: 4, language: "English", size: 11363, available: true, published_on: "30 Jan 2002", price: 180, discount: 1, pages: 1134, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/dbms.pdf", image_src: "assets/images/books/dbms.jpeg", name: "Database Management Systems", description: "Learn a lot about dbms", edition: 6, language: "English", size: 8694, available: true, published_on: "30 Jan 2002", price: 310, discount: 19, pages: 1172, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/digital_electronics.pdf", image_src: "assets/images/books/de.jpg", name: "Digital Electronics", description: "In depth knowledge on digital electronics", edition: 5, language: "English", size: 3135, available: true, published_on: "30 Jan 2002", price: 239, discount: 7, pages: 547, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/Introduction_To_Automata_Theory.pdf", image_src: "assets/images/books/toc.jpg", name: "Introduction To Automata Theory", description: "Best book on Theory Of Computation", edition: 2, language: "English", size: 22123, available: true, published_on: "30 Jan 2002", price: 123, discount: 7, pages: 521, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/java.pdf", image_src: "assets/images/books/java.jpg", name: "Programming in Java", description: "Learn Java step by step", edition: 7, language: "English", size: 6690, available: true, published_on: "30 Jan 2002", price: 312, discount: 17, pages: 1024, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/mpmc8051.pdf", image_src: "assets/images/books/mpmc.jpeg", name: "Micro controller 8051 - Kenneth Ayala", description: "Detailed explanations on 8051 microcontroller", edition: 1, language: "English", size: 6906, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 256, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },


  {url: "assets/Mechanical/Automotive_Engineering_Fundamentals.pdf", image_src: "assets/images/books/AIF.jpg", name: "Automotive Engineering Fundamentals", description: "A detailed explanations on automotive fundamentals", edition: 3, language: "English", size: 25949, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 636, category_id: Category.find(id=9).id, sold: 812, allow_ratings: true },

  {url: "assets/Mechanical/Automotive_Technology_A_Systems_Approach.pdf", image_src: "assets/images/books/ATAS.jpg", name: "Automotive Technology A Systems Approach", description: "Great explanations on automotive technologies", edition: 5, language: "English", size: 155378, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 1648, category_id: Category.find(id=9).id, sold: 812, allow_ratings: true },

  {url: "assets/Mechanical/Engineering_Fundamentals_of_Internal_Combustion_Engines.pdf", image_src: "assets/images/books/ICI.jpg", name: "Engineering Fundamentals of Internal Combustion Engines", description: "A detailed explanations on internal combustion engines", edition: 1, language: "English", size: 7038, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 411, category_id: Category.find(id=9).id, sold: 812, allow_ratings: true },

  {url: "assets/Mechanical/Internal_Combustion_Engine_Fundamentals.pdf", image_src: "assets/images/books/ICE.jpg", name: "Internal Combustion Engine Fundamentals", description: "A detailed explanations on internal combustion engines", edition: 1, language: "English", size: 44352, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 930, category_id: Category.find(id=9).id, sold: 812, allow_ratings: true },



  {url: "assets/CSE/AI.djvu", image_src: "assets/images/books/ai.jpg", name: "Artificial Intelligence", description: "Best book on AI", edition: 1, language: "English", size: 11863, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 932, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/compiler.pdf", image_src: "assets/images/books/compiler.jpg", name: "Compiler Engineering", description: "Gives the reader a great understanding on compiler engineering", edition: 1, language: "English", size: 2309, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 354, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/cryptography.pdf", image_src: "assets/images/books/crypt.jpeg", name: "Cryptography and Network Security", description: "Cool book on cryptography", edition: 5, language: "English", size: 8366, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 719, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/data_mining.pdf", image_src: "assets/images/books/mining.jpeg", name: "Data mining and data warehousing", description: "Is your data safe?", edition: 3, language: "English", size: 13134, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 703, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/data_structures.pdf", image_src: "assets/images/books/ds.jpeg", name: "Data structures", description: "Understand the data structures", edition: 2, language: "English", size: 4930, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 720, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/distributed_systems.pdf", image_src: "assets/images/books/distr.jpg", name: "Distributed Systems", description: "What are the advantages of distributed systems? Are they fast? Get to know them!", edition: 2, language: "English", size: 10044, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 705, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/linux.pdf", image_src: "assets/images/books/linux.jpg", name: "Linux Programming Bible", description: "Gives the reader a good understanding on Linux environment", edition: 1, language: "English", size: 3814, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 517, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/mpm.pdf", image_src: "assets/images/books/mp.jpg", name: "Modern Embedded Comouting", description: "Understand the embedded computing techniques", edition: 1, language: "English", size: 11889, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 518, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/natural_language.pdf", image_src: "assets/images/books/nlp.jpeg", name: "Natural language processing", description: "Know about the natural language processing", edition: 1, language: "English", size: 4304, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 479, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/os.pdf", image_src: "assets/images/books/os.jpg", name: "Operating Systems", description: "What does a computer actually do", edition: 6, language: "English", size: 4187, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 822, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/os2.pdf", image_src: "assets/images/books/os2.jpeg", name: "Operating System Concepts", description: "Know about the concepts of an operating system", edition: 7, language: "English", size: 828, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 150, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true },

  {url: "assets/CSE/parallel_distributed_systems.pdf", image_src: "assets/images/books/cuda.jpg", name: "Parallel and Distributed Systems", description: "Get to know about the parallel and distributed systems using CUDA", edition: 1, language: "English", size: 2076, available: true, published_on: "30 Jan 2002", price: 102, discount: 12, pages: 290, category_id: Category.find(id=8).id, sold: 812, allow_ratings: true }

])

Author.create({first_name: "Thomas", last_name: "Corman", awards: "Fun guy"})
AuthorBookRelation.create({author_id: Author.first.id, book_id: Book.first.id})
User.create({first_name: "Raghav", last_name: "Vc", email: "vc.raghav@gmail.com", password: "demodemo", phone: 9600129789})
Order.create({user_id: User.first.id, book_id: Book.first.id, price: Book.first.price, payment_status: "Done", paid_on: Time.now})
Wallet.create({user_id: User.first, card_name: "Super card", card_type: "Visa", card_num: 4242424242424242, card_cvv: 234, billing_address: "Anywhere", balance: 1000})

Author.create({first_name: "David", last_name: "Black", awards: "Cool author"})
AuthorBookRelation.create({author_id: Author.second.id, book_id: Book.second.id})
User.create({first_name: "Venkatesh", last_name: "Ma", email: "geekvenk@gmail.com", password: "demodemo", phone: 1234567890})
Order.create({user_id: User.second, book_id: Book.second, price: Book.second.price, payment_status: "Done", paid_on: Time.now})
Wallet.create({user_id: User.second, card_name: "Small card", card_type: "Master", card_num: 4242424242424242, card_cvv: 234, billing_address: "Home", balance: 600})

Author.create({first_name: "Michael", last_name: "Hartl", awards: "Nerd guy"})
AuthorBookRelation.create({author_id: Author.third.id, book_id: Book.third.id})
User.create({first_name: "Srinivasa", last_name: "Varadhan", email: "srinivas.varadhan@gmail.com", password: "demodemo", phone: 9123456712})
Order.create({user_id: User.third, book_id: Book.third, price: Book.third.price, payment_status: "Done", paid_on: Time.now})
Wallet.create({user_id: User.third, card_name: "Level card", card_type: "Visa", card_num: 4242424242424242, card_cvv: 123, billing_address: "LOL", balance: 2000})

Author.create({first_name: "Das", last_name: "Gupta", awards: "Best book on Maths"})
AuthorBookRelation.create({author_id: 4, book_id: 9})

Author.create({first_name: "GKP", last_name: "", awards: "Gateway to success"})
AuthorBookRelation.create({author_id: 5, book_id: 8})

Author.create({first_name: "James", last_name: "Wilson", awards: "Best GRE book of the year"})
AuthorBookRelation.create({author_id: 6, book_id: 7})
  
Author.create({first_name: "Amish", last_name: "Tripathi", awards: "Best Indian writer on mythology"})
AuthorBookRelation.create({author_id: 7, book_id: 6})

AuthorBookRelation.create({author_id: 7, book_id: 5})
AuthorBookRelation.create({author_id: 7, book_id: 4})

Author.create({first_name: "Ruskin", last_name: "Bond", awards: "Cool children stories"})
AuthorBookRelation.create({author_id: 8, book_id: 10})

Author.create({first_name: "RK", last_name: "Narayan", awards: "Best writer of children stories"})
AuthorBookRelation.create({author_id: 9, book_id: 11})

Author.create({first_name: "Sudha", last_name: "Murthy", awards: ""})
AuthorBookRelation.create({author_id: 10, book_id: 12})

Author.create({first_name: "Scott", last_name: "Peterson", awards: "Best Comic Award"})
AuthorBookRelation.create({author_id: 11, book_id: 13})

Author.create({first_name: "Georges", last_name: "Remi", awards: ""})
AuthorBookRelation.create({author_id: 12, book_id: 14})

Author.create({first_name: "Jeff", last_name: "Kinney", awards: "Best Comic Writer - USA"})
AuthorBookRelation.create({author_id: 13, book_id: 15})

Author.create({first_name: "Abdul", last_name: "Kalam", awards: ""})
AuthorBookRelation.create({author_id: 14, book_id: 16})

Author.create({first_name: "Adolf", last_name: "Hitler", awards: ""})
AuthorBookRelation.create({author_id: 15, book_id: 17})

Author.create({first_name: "Anne", last_name: "Frank", awards: ""})
AuthorBookRelation.create({author_id: 16, book_id: 18})

AuthorBookRelation.create({author_id: 1, book_id: 19})

Author.create({first_name: "Raghav", last_name: "Vc", awards: "Best Rails developer award"})
AuthorBookRelation.create({author_id: 17, book_id: 20})

Author.create({first_name: "Jeffery", last_name: "Archer", awards: "Best story writer award"})
AuthorBookRelation.create({author_id: 18, book_id: 21})

Author.create({first_name: "Sachin", last_name: "Tendulkar", awards: "Master Blaster"})
AuthorBookRelation.create({author_id: 19, book_id: 22})

AuthorBookRelation.create({author_id: 7, book_id: 23})

Author.create({first_name: "THG", last_name: "Megson", awards: ""})
AuthorBookRelation.create({author_id: 20, book_id: 24})

Author.create({first_name: "Frank", last_name: "White", awards: ""})
AuthorBookRelation.create({author_id: 21, book_id: 25})

Author.create({first_name: "John", last_name: "Anderson", awards: ""})
AuthorBookRelation.create({author_id: 22, book_id: 26})

Author.create({first_name: "Saravana", last_name: "Muttoo", awards: ""})
AuthorBookRelation.create({author_id: 23, book_id: 27})

AuthorBookRelation.create({author_id: 22, book_id: 28})

Author.create({first_name: "William", last_name: "Stallings", awards: ""})
AuthorBookRelation.create({author_id: 24, book_id: 29})

Author.create({first_name: "Donald", last_name: "Hearn", awards: ""})
AuthorBookRelation.create({author_id: 25, book_id: 30})

Author.create({first_name: "Andrew", last_name: "Tanenbaum", awards: ""})
AuthorBookRelation.create({author_id: 26, book_id: 31})

Author.create({first_name: "Behrouz", last_name: "Fourouzan", awards: ""})
AuthorBookRelation.create({author_id: 27, book_id: 32})

Author.create({first_name: "Ramez", last_name: "Elmasri", awards: ""})
AuthorBookRelation.create({author_id: 28, book_id: 33})

Author.create({first_name: "Morris", last_name: "Mano", awards: ""})
AuthorBookRelation.create({author_id: 29, book_id: 34})

Author.create({first_name: "John", last_name: "Hopcroft", awards: ""})
AuthorBookRelation.create({author_id: 30, book_id: 35})

Author.create({first_name: "Herbert", last_name: "Schildt", awards: ""})
AuthorBookRelation.create({author_id: 31, book_id: 36})

Author.create({first_name: "Kenneth", last_name: "Ayala", awards: ""})
AuthorBookRelation.create({author_id: 32, book_id: 37})

Author.create({first_name: "Richard", last_name: "Stone", awards: ""})
AuthorBookRelation.create({author_id: 33, book_id: 38})

Author.create({first_name: "Jack", last_name: "Erjavek", awards: ""})
AuthorBookRelation.create({author_id: 34, book_id: 39})

Author.create({first_name: "Willard", last_name: "Pulkrabek", awards: ""})
AuthorBookRelation.create({author_id: 35, book_id: 40})

Author.create({first_name: "John", last_name: "Heywood", awards: ""})
AuthorBookRelation.create({author_id: 36, book_id: 41})

Author.create({first_name: "Stuart", last_name: "Russel", awards: ""})
AuthorBookRelation.create({author_id: 37, book_id: 42})

Author.create({first_name: "Keith", last_name: "Cooper", awards: ""})
AuthorBookRelation.create({author_id: 38, book_id: 43})

AuthorBookRelation.create({author_id: 24, book_id: 44})

Author.create({first_name: "Jiawei", last_name: "Han", awards: ""})
AuthorBookRelation.create({author_id: 39, book_id: 45})

AuthorBookRelation.create({author_id: 27, book_id: 46})

AuthorBookRelation.create({author_id: 26, book_id: 47})

Author.create({first_name: "John", last_name: "Goerzen", awards: ""})
AuthorBookRelation.create({author_id: 40, book_id: 48})

Author.create({first_name: "Peter", last_name: "Barry", awards: ""})
AuthorBookRelation.create({author_id: 41, book_id: 49})

Author.create({first_name: "Steven", last_name: "Bird", awards: ""})
AuthorBookRelation.create({author_id: 42, book_id: 50})

AuthorBookRelation.create({author_id: 24, book_id: 51})

Author.create({first_name: "Abraham", last_name: "Silberschatz", awards: ""})
AuthorBookRelation.create({author_id: 43, book_id: 52})

Author.create({first_name: "Jason", last_name: "Sanders", awards: ""})
AuthorBookRelation.create({author_id: 44, book_id: 53})